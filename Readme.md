yii2-docusign
====================

The **yii2-docusign** extension is a Yii2 component which integrates DocuSign's Restful API.

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

## Configuration

Add **docusign** component into your components config.

### Example

```php
'components' => [
    // ...
    'docusign' => [
        'integrator_key' => 'YOUR KEY HERE',
        'email' => '',
        'password' => '',
        'version' => '',
        'environment' => '',
        'account_id' => ''
    ],
],
```
## License

**yii2-docusign** is released under the BSD 3-Clause License.
