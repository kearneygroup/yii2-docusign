<?php
/**
 * @author James Lewis <james.lewis@kearneygroup.com.au>
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package yii2-docusign
 * @version 1.0
 */

/*
 * Copyright 2013 DocuSign Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace kearneygroup\docusign;

use Yii;
use kearneygroup\docusign\exception\DocuSign_AuthException;
use kearneygroup\docusign\exception\DocuSign_Exception;
use kearneygroup\docusign\exception\DocuSign_IOException;
use kearneygroup\docusign\io\DocuSign_Creds;
use kearneygroup\docusign\io\DocuSign_CurlIO;
use kearneygroup\docusign\model\DocuSign_Model;
use kearneygroup\docusign\service\DocuSign_Resource;

class DocuSign_Client {

    // The DocuSign Credentials
    public $creds;

    // The version of DocuSign API
    public $version;

    // The DocuSign Environment
    public $environment;

    // The base url of the DocuSign Account
    public $baseURL;

    // The DocuSign Account Id
    public $accountID;

    // The DocuSign_CurlIO class
    public $curl;

    // The flag indicating if it has multiple DocuSign accounts
    public $hasMultipleAccounts = false;

    public $hasError = false;

    public $errorMessage = '';

	public function __construct($clientConfig = null) {

        $this->performSystemChecks();

        if(is_null($clientConfig)){
            $clientConfig = Yii::$app->components['docusign'];
            $clientConfig = new DocuSign_ClientConfig($clientConfig['integrator_key'],$clientConfig['email'],$clientConfig['password'],$clientConfig['version'],$clientConfig['environment'],$clientConfig['account_id']);
        }elseif(is_array($clientConfig)){
            $clientConfig = new DocuSign_ClientConfig($clientConfig['integrator_key'],$clientConfig['email'],$clientConfig['password'],$clientConfig['version'],$clientConfig['environment'],$clientConfig['account_id']);
        }

        if(get_class($clientConfig) != 'DocuSign_ClientConfig'){
            throw new DocuSign_Exception('DocuSign configuration invalid.');
        }

        if(isset($clientConfig->version)) $this->version = $clientConfig->version;
        if(isset($clientConfig->environment)) $this->environment = $clientConfig->environment;
        if(!empty($clientConfig->account_id)) $this->accountID = $clientConfig->account_id;

        $this->creds = new DocuSign_Creds($clientConfig->integrator_key, $clientConfig->email, $clientConfig->password);

        if(!is_null($clientConfig->email) && !is_null($clientConfig->password)){
            $this->testCredentials();
        }
	}

    private function performSystemChecks(){
        if (! function_exists('json_decode')) {
            throw new DocuSign_Exception('DocuSign PHP API Client requires the JSON PHP extension');
        }

        if (! function_exists('curl_version')) {
            throw new DocuSign_Exception('DocuSign PHP API Client requires the PHP Client URL Library');
        }

        if (! function_exists('http_build_query')) {
            throw new DocuSign_Exception('DocuSign PHP API Client requires http_build_query()');
        }

        if (! ini_get('date.timezone') && function_exists('date_default_timezone_set')) {
            date_default_timezone_set('UTC');
        }
        return true;
    }
	
	public function authenticate() {
		$url = 'https://' . $this->environment . '.docusign.net/restapi/' . $this->version . '/login_information';
		try {
		 	$response = $this->curl->makeRequest($url, 'GET', $this->getHeaders());
		} catch (DocuSign_AuthException $e) {
			$this->hasError = true;
			$this->errorMessage = $e->getMessage();
			return;
		}
		
		if( count($response->loginAccounts) > 1 ) $this->hasMultipleAccounts = true;
		$defaultBaseURL = '';
		$defaultAccountID = '';
		foreach($response->loginAccounts as $account) {
			if( !empty($this->accountID) ) {
				if( $this->accountID == $account->accountId ) {
					$this->baseURL = $account->baseUrl;
					break;
				}
			}
			if( $account->isDefault == 'true' ) { 
				$defaultBaseURL = $account->baseUrl;
				$defaultAccountID = $account->accountId;
			}
		}
		if(empty($this->baseURL)) {
			$this->baseURL = $defaultBaseURL;
			$this->accountID = $defaultAccountID;
		}

		return $response;
	}

	public function getCreds() { return $this->creds; }
	public function getVersion() { return $this->version; }
	public function getEnvironment() { return $this->environment; }
	public function getBaseURL() { return $this->baseURL; }
	public function getAccountID() { return $this->accountID; }
	public function getCUrl() { return $this->curl; }
	public function hasMultipleAccounts() { return $this->hasMultipleAccounts; }
	public function hasError() { return $this->hasError; }
	public function getErrorMessage() { return $this->errorMessage; }
	public function getHeaders($accept = 'Accept: application/json', $contentType = 'Content-Type: application/json') {
        if($this->creds->isEmpty()){
            throw new DocuSign_Exception('No DocuSign user credentials specified.');
        }
		return array(
			'X-DocuSign-Authentication: <DocuSignCredentials><Username>' . $this->creds->getEmail() . '</Username><Password>' . $this->creds->getPassword() . '</Password><IntegratorKey>' . $this->creds->getIntegratorKey() . '</IntegratorKey></DocuSignCredentials>',
			$accept,
			$contentType
		);
	}
	public function getSoboHeaders($soboUser, $accept = 'Accept: application/json', $contentType = 'Content-Type: application/json') {
        if($this->creds->isEmpty()){
            throw new DocuSign_Exception('No DocuSign user credentials specified.');
        }
		return array(
			'X-DocuSign-Authentication: <DocuSignCredentials><SendOnBehalfOf>' . $soboUser . '</SendOnBehalfOf><Username>' . $this->creds->getEmail() . '</Username><Password>' . $this->creds->getPassword() . '</Password><IntegratorKey>' . $this->creds->getIntegratorKey() . '</IntegratorKey></DocuSignCredentials>',
			$accept,
			$contentType
		);
	}
    public function switchUser($email,$password){
        $this->creds->setEmail($email);
        $this->creds->setPassword($password);
    }
    private function testCredentials(){
        $this->curl = new DocuSign_CurlIO();
        if(!$this->creds->isEmpty())
            self::authenticate();
        else {
            // you can potentially make this a warning instead of error if you'd like, depends on
            // how you want to handle missing api credentials...
            $this->hasError = true;
            $this->errorMessage = "One or more missing config settings found.";
        }
    }
}

?>