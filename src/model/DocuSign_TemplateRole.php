<?php
/*
 * Copyright 2013 DocuSign Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace kearneygroup\docusign\model;

class DocuSign_TemplateRole extends DocuSign_Model {
    private $roleName;
    private $name;
    private $email;

    public function __construct($roleName, $name, $email) {
        if( isset($roleName) ) $this->roleName = $roleName;
        if( isset($name) ) $this->name = $name;
        if( isset($email) ) $this->email = $email;
    }

    public function setRoleName($roleName) { $this->roleName = $roleName; }
    public function getRolename() { return $this->roleName; }
    public function setName($name) { $this->name = $name; }
    public function getName() { return $this->name; }
    public function setEmail($email) { $this->email = $email; }
    public function getEmail() { return $this->email; }
}

?>