<?php

namespace kearneygroup\docusign;

class DocuSign_ClientConfig {
    // The DocuSign Integrator's Key
    public $integrator_key = null;

    // The Docusign Account Email
    public $email = null;

    // The Docusign Account password or API password
    public $password = null;

    // The version of DocuSign API (Ex: v1, v2)
    public $version = null;

    // The DocuSign Environment (Ex: demo, test, www)
    public $environment = null;

    // The DocuSign Account Id (Optional)
    // For multiple accounts user:
    //   - if it's empty, the default account will be used
    //   - otherwise, the DocuSign account with this account id will be used
    public $account_id = null;

    public function __construct($integrator_key,$email=null,$password=null,$version='v2',$environment='www',$account_id=null){
        $this->integrator_key = $integrator_key;
        $this->email = $email;
        $this->password = $password;
        $this->version = $version;
        $this->environment = $environment;
        $this->account_id = $account_id;
    }
}
